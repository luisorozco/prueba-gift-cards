

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json())
 
const cors = require('cors')
const corsOptions = {
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200
}
 
app.use(cors(corsOptions))
 
const db = require('./app/config/db.config.js');
  
// force: true will drop the table if it already exists
db.sequelize.sync({force: true}).then(() => {
  console.log('Drop and Resync with { force: true }');
  initial();
});
 
require('./app/route/picture.route.js')(app);
require('./app/route/picture_type.route.js')(app);
require('./app/route/giftcard.route.js')(app);

// Create a Server
var server = app.listen(8080, function () {
 
  let host = server.address().address
  let port = server.address().port
 
  console.log("App listening at http://%s:%s", host, port);
})
 
function initial(){

  let picture_types = [
    {
      description: "Unicolores"
     },
     {
      description: "Geométricas"
     },
     {
      description: "Paisajes"
     },
     {
      description: "Animales"
     }
  ]
 
  // Init data -> save to MySQL
  const Picture_type = db.picture_types;
  for (let i = 0; i < picture_types.length; i++) { 
    Picture_type.create(picture_types[i]);  
  }


  let pictures = [
    {
      id_picture_type: 1,
      link: "https://i.pinimg.com/236x/09/60/33/0960339ae7ce6903f0dd0623621679da.jpg?nii=t"
    },
    {
      id_picture_type: 1,
      link:"https://i.pinimg.com/236x/9b/98/b9/9b98b9793946d1d683e4e3dff1a3c190--ely-velvet.jpg"
    },
    {
      id_picture_type: 1,
      link: "https://i.pinimg.com/236x/e1/60/42/e16042db55087bd050a7ae2dd63952bb.jpg"
    },
    {
      id_picture_type: 2,
      link: "https://i1.wp.com/www.jumabu.com/es/wp-content/images/fondo-geometrico-colores-triangulos.jpg?fit=626%2C626&ssl=1"
    },
    {
      id_picture_type: 2,
      link: "https://image.freepik.com/vector-gratis/diseno-fondo-figuras-geometricas_24908-22204.jpg"
    },
    {
      id_picture_type: 2,
      link: "https://i.pinimg.com/originals/54/eb/80/54eb805d3abe28a562059d54fb0896bd.jpg"
    },
    {
      id_picture_type: 3,
      link: "http://1.bp.blogspot.com/_NIsy0d8Bc-I/TBkn9z9W6AI/AAAAAAAAADk/cMzmlUmQNz4/s1600/paisajes-caida-oregon.jpg"
    },
    {
      id_picture_type: 3,
      link: "https://image.freepik.com/foto-gratis/monte-bellos-paisajes-fujisan-al-atardecer_30824-25.jpg"
    },
    {
      id_picture_type: 3,
      link: "https://haciendofotos.com/wp-content/uploads/las-mejores-fotos-de-paisajes-nevados-arboles-600x338.jpg"
    },
    {
      id_picture_type: 4,
      link: "http://www.elmundodelosanimales.com/wallpapers/grandes/ardilla.jpg"
    },
    {
      id_picture_type: 4,
      link: "https://www.fondosanimales.com/Minis/osos-polares.jpg"
    },
    {
      id_picture_type: 4,
      link: "https://todastusfrases.com/wp-content/uploads/2018/10/Fondos-de-pantalla-de-animales-graciosos-y-divertidos-27.jpg"
     },
      {
        id_picture_type: 4,
      link: "https://k61.kn3.net/taringa/2/A/4/5/4/F/JORGELS5321/EC1.jpg"
    }
  ]
 
  // Init data -> save to MySQL
  const Picture = db.pictures;
  for (let i = 0; i < pictures.length; i++) { 
    Picture.create(pictures[i]);  
  }

  let giftcards = [
    {
      id_picture:"5",
      amount: 250,
      message: "Feliz cumpleaños!",
      recipient_name:"Mariangel Guedez",
      recipient_email_address:"mariangel@gmail.com",
      recipient_mobile_number:"3321234578",
      purchaser_name:"Marisol Alvarado",
      delivery_date:"2019-07-15 21:37:43.123-05"   
    },
    {
      id_picture:"9",
      amount: 100,
      message: "Congratulations!",
      recipient_name:"Genesis Alvarez",
      recipient_email_address:"gal@hotmail.com",
      recipient_mobile_number:"04145478452",
      purchaser_name:"Andreina Diaz",
      delivery_date:"2019-07-18 08:37:43.123-05"   
    },
    {
      id_picture:"9",
      amount: 50,
      message: "Sorpresa",
      recipient_name:"Junior Hernandez",
      recipient_email_address:"eahm@gmail.com",
      recipient_mobile_number:"3003040031",
      purchaser_name:"Rebeca Martinez",
      delivery_date:"2019-07-18 08:37:43.123-05"   
    },
    {
      id_picture:"1",
      amount: 20,
      message: "Hola",
      recipient_name:"Luis Orozco",
      recipient_email_address:"a@gmail.com",
      recipient_mobile_number:"123456789",
      purchaser_name:"Nancy Torrealba",
      delivery_date:"2019-07-17 08:37:43.123-05"   
    },
    {
      id_picture:"2",
      amount: 300,
      message: "Cuidate",
      recipient_name:"Ana de Palma",
      recipient_email_address:"b@gmail.com",
      recipient_mobile_number:"321654987",
      purchaser_name:"Luis Almao",
      delivery_date:"2019-07-07 08:37:43.123-05"   
    },
    {
      id_picture:"3",
      amount: 400,
      message: "Hello",
      recipient_name:"Marianny Sanchez",
      recipient_email_address:"c@hotmail.com",
      recipient_mobile_number:"147258369",
      purchaser_name:"Elena Torres",
      delivery_date:"2019-06-18 08:37:43.123-05"   
    },
    {
      id_picture:"4",
      amount: 500,
      message: "Te quiero",
      recipient_name:"Iris Muijica",
      recipient_email_address:"d@gmail.com",
      recipient_mobile_number:"987654321",
      purchaser_name:"Nury Amaro",
      delivery_date:"2019-07-14 08:37:43.123-05"   
    }
    
  ]
 
  // Init data -> save to MySQL
  const Giftcard = db.giftcards;
  for (let i = 0; i < giftcards.length; i++) { 
    Giftcard.create(giftcards[i]);  
  }

}