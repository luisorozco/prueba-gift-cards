const db = require('../config/db.config.js');
const Picture = db.pictures;

// Guardar imagen
exports.create = (req, res) => {	
	// 
	Picture.create({
				"id_picture_type": req.body.id_picture_type, 
				"link": req.body.link
			}).then(Picture => {		
			// 
			res.json(picture);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// Todas las Imagenes
exports.findAll = (req, res) => {
	Picture.findAll().then(pictures => {
			// 
			res.json(pictures.sort(function(c1, c2){return c1.id - c2.id}));
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};

// buscar imagen
exports.findById = (req, res) => {	
	Picture.findById(req.params.id).then(picture => {
			res.json(picture);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// Actualizar imagen
exports.update = (req, res) => {
	const id = req.body.id;
	Picture.update( req.body, 
			{ where: {id: id} }).then(() => {
				res.status(200).json( { mgs: "Imagen actualizada exitosamente-> id de la imagen= " + id } );
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
};

// Eliminar una imagen por Id
exports.delete = (req, res) => {
	const id = req.params.id;
	Picture.destroy({
			where: { id: id }
		}).then(() => {
			res.status(200).json( { msg: 'Imagen eliminada exitosamente-> id de la imagen= ' + id } );
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};