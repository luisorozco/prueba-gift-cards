const db = require('../config/db.config.js');
const Giftcard = db.giftcards;

// Crear una GiftCard
exports.create = (req, res) => {	
	
	Giftcard.create({
				"id_picture": req.body.id_picture_type, 
                "amount": req.body.amount, 
                "message": req.body.message, 
                "recipient_name": req.body.recipient_name, 
                "recipient_email_address": req.body.recipient_email_address, 
                "recipient_mobile_number": req.body.recipient_mobile_number, 
                "purchaser_name": req.body.purchaser_name, 
                "delivery_date": req.body.delivery_date
			}).then(giftcard => {		
			
			res.json(giftcard);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// todas las GiftCards
exports.findAll = (req, res) => {
	Giftcard.findAll().then(giftcards => {
			// 
			res.json(giftcards.sort(function(c1, c2){return c1.id - c2.id}));
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};

// Encontrar una GiftCards
exports.findById = (req, res) => {	
	Giftcard.findById(req.params.id).then(giftcard => {
			res.json(giftcard);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
//actualizar una GiftCard
exports.update = (req, res) => {
	const id = req.body.id;
	Giftcard.update( req.body, 
			{ where: {id: id} }).then(() => {
				res.status(200).json( { mgs: "GiftCard actualizada correctamente -> GiftCard Id = " + id } );
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
};

// Eliminar una giftcard por el Id
exports.delete = (req, res) => {
	const id = req.params.id;
	Giftcard.destroy({
			where: { id: id }
		}).then(() => {
			res.status(200).json( { msg: 'Giftcard Eliminada correctamente -> Giftcard Id = ' + id } );
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};