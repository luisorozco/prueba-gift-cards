const db = require('../config/db.config.js');
const Picture_type = db.picture_types;

// Guardar un tipo de imagen
exports.create = (req, res) => {	
	// 
	Picture_type.create({
				"description": req.body.description, 
			}).then(Picture_type => {		
			// 
			res.json(picture_type);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// Todas los tipos de Imagenes
exports.findAll = (req, res) => {
	Picture_type.findAll().then(picture_types => {
			// 
			res.json(picture_types.sort(function(c1, c2){return c1.id - c2.id}));
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};

// buscar un tipo de imagen
exports.findById = (req, res) => {	
	Picture_type.findById(req.params.id).then(picture_type => {
			res.json(picture_type);
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};
 
// Actualizar un tipo imagen
exports.update = (req, res) => {
	const id = req.body.id;
	Picture_type.update( req.body, 
			{ where: {id: id} }).then(() => {
				res.status(200).json( { mgs: "Tipo de imagen actualizado exitosamente-> id del tipo de imagen= " + id } );
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
};

// Eliminar una tipo de imagen por Id
exports.delete = (req, res) => {
	const id = req.params.id;
	Picture_type.destroy({
			where: { id: id }
		}).then(() => {
			res.status(200).json( { msg: 'Tipo de imagen eliminado exitosamente-> id del tipo imagen= ' + id } );
		}).catch(err => {
			console.log(err);
			res.status(500).json({msg: "error", details: err});
		});
};