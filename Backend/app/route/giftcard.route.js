module.exports = function(app) {
    const giftcards = require('../controller/giftcard.controller.js');
 
    // Crear una giftcard
    app.post('/api/giftcards', giftcards.create);
 
    // Todas las giftcards
    app.get('/api/giftcards', giftcards.findAll);
 
    // Buscar una giftcard por el Id
    app.get('/api/giftcards/:id', giftcards.findById);
 
    // Actualizar una giftcard por el Id
    app.put('/api/giftcards', giftcards.update);
 
    // Eliminar una giftcard por el Id
    app.delete('/api/giftcards/:id', giftcards.delete);
}