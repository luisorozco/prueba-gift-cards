module.exports = function(app) {
    const picture_types = require('../controller/picture_type.controller.js');
 
    // Crear un tipo de imagen
    app.post('/api/picture_types', picture_types.create);
 
    // Todos las tipos de imagenes
    app.get('/api/picture_types', picture_types.findAll);
 
    // Buscar un tipo de imagen Id
    app.get('/api/picture_types/:id', picture_types.findById);
 
    // Actualizar un tipo de imagen por Id
    app.put('/api/picture_types', picture_types.update);
 
    // Eliminar un tipo de imagen por Id
    app.delete('/api/picture_types/:id', picture_types.delete);
}