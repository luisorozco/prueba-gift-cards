module.exports = function(app) {
    const pictures = require('../controller/picture.controller.js');
 
    // Crear una imagen
    app.post('/api/pictures', pictures.create);
 
    // Todas las imagenes
    app.get('/api/pictures', pictures.findAll);
 
    // Buscar una imagen por Id
    app.get('/api/pictures/:id', pictures.findById);
 
    // Actualizar una imagen por Id
    app.put('/api/pictures', pictures.update);
 
    // Eliminar una imagen por Id
    app.delete('/api/pictures/:id', pictures.delete);
}