module.exports = (sequelize, Sequelize) => {
	const Giftcard = sequelize.define('giftcard', {
        id_picture: {
			type: Sequelize.INTEGER
        },
        amount: {
            type: Sequelize.REAL
        },
        message: {
			type: Sequelize.STRING
	    },
	    recipient_name: {
			type: Sequelize.STRING
	    },
	    recipient_email_address: {
            type: Sequelize.STRING
        },
        recipient_mobile_number: {
            type: Sequelize.STRING
        },
        purchaser_name: {
            type: Sequelize.STRING
        },
        delivery_date: {
            type: Sequelize.DATE
        }
	});
	
	return Giftcard;
}