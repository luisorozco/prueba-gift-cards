module.exports = (sequelize, Sequelize) => {
	const Picture = sequelize.define('picture', {
	  id_picture_type: {
			type: Sequelize.INTEGER
	  },
	  link: {
			type: Sequelize.STRING
	  }
	});
	
	return Picture;
}