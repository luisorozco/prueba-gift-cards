import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GiftcardComponent } from '../giftcard/giftcard.component';
import { AddGiftcardComponent } from '../add-giftcard/add-giftcard.component';

const routes: Routes = [
  
  { 
    path: 'giftcards', 
    component: GiftcardComponent 
  },
  { 
    path: 'giftcard/add', 
    component: AddGiftcardComponent 
  },

   
   { 
     path: '', 
     redirectTo: 'giftcard/add', 
     pathMatch: 'full'
   }, 
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}