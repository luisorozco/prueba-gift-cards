import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Picture } from './picture';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PictureService {
  private picturesUrl = 'http://localhost:8080/api/pictures';  // URL to web api
  constructor( 
    private http: HttpClient
  ) { }

  getPictures (): Observable<Picture[]> {
    return this.http.get<Picture[]>(this.picturesUrl)
  }

  getPicture(id: number): Observable<Picture> {
    const url = `${this.picturesUrl}/${id}`;
    return this.http.get<Picture>(url);
  }

  addPicture (picture: Picture): Observable<Picture> {
    return this.http.post<Picture>(this.picturesUrl, picture, httpOptions);
  }

  deletePicture (picture: Picture | number): Observable<Picture> {
    const id = typeof picture === 'number' ? picture : picture.id;
    const url = `${this.picturesUrl}/${id}`;

    return this.http.delete<Picture>(url, httpOptions);
  }

  updatePicture (picture: Picture): Observable<any> {
    return this.http.put(this.picturesUrl, picture, httpOptions);
  }
}