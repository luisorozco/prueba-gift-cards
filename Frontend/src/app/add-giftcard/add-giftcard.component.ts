import { Component, OnInit, Renderer2, AfterViewInit} from '@angular/core';
import { Giftcard } from '../giftcard';
import { GiftcardService } from '../giftcard.service';
import { Picture } from '../picture';
import { PictureService} from '../picture.service';
import { Picture_type} from '../picture_type'
import { Picture_typeService} from '../picture_type.service';


import { Location } from '@angular/common';

@Component({
  selector: 'app-add-giftcard',
  templateUrl: './add-giftcard.component.html',
  styleUrls: ['./add-giftcard.component.css']
})

export class AddGiftcardComponent {
  pictures: Picture[];
  picture_types: Picture_type[];
  giftcard = new Giftcard();
  submitted = false;


  constructor(
    private giftcardService: GiftcardService,
    private location: Location,
    private pictureService: PictureService,
    private picture_typeService: Picture_typeService,
    private renderer:Renderer2
  ) { }

  ngOnInit(): void {
    this.getPictures();
    this.getPictures_type();

 }




 getPictures() {
   return this.pictureService.getPictures()
              .subscribe(
               pictures => {
                 console.log(pictures);
                 this.pictures = pictures
                }
               );
}

getPictures_type() {
  return this.picture_typeService.getPicture_types()
             .subscribe(
              picture_types => {
                console.log(picture_types);
                this.picture_types = picture_types
               }
              );
}
  newGiftcard(): void {
    this.submitted = false;
    this.giftcard = new Giftcard();
  }

 addGiftcard() {
   this.submitted = true;
   this.save();
 }

  goBack(): void {
    this.location.back();
  }

  private save(): void {
    console.log(this.giftcard);
    this.giftcardService.addGiftcard(this.giftcard)
        .subscribe();
  }

}
