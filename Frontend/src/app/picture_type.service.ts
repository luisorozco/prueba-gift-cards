import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Picture_type } from './picture_type';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class Picture_typeService {
  private picture_typesUrl = 'http://localhost:8080/api/picture_types';  // URL to web api
  constructor( 
    private http: HttpClient
  ) { }

  getPicture_types (): Observable<Picture_type[]> {
    return this.http.get<Picture_type[]>(this.picture_typesUrl)
  }

  getPicture_type(id: number): Observable<Picture_type> {
    const url = `${this.picture_typesUrl}/${id}`;
    return this.http.get<Picture_type>(url);
  }

  addPicture_type (picture_type: Picture_type): Observable<Picture_type> {
    return this.http.post<Picture_type>(this.picture_typesUrl, picture_type, httpOptions);
  }

  deletePicture_type (picture_type: Picture_type | number): Observable<Picture_type> {
    const id = typeof picture_type === 'number' ? picture_type : picture_type.id;
    const url = `${this.picture_typesUrl}/${id}`;

    return this.http.delete<Picture_type>(url, httpOptions);
  }

  updatePicture_type (picture_type: Picture_type): Observable<any> {
    return this.http.put(this.picture_typesUrl, picture_type, httpOptions);
  }
}