import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Giftcard } from './giftcard';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class GiftcardService {
  private giftcardsUrl = 'http://localhost:8080/api/giftcards';  // URL to web api
  constructor( 
    private http: HttpClient
  ) { }

  getGiftcards (): Observable<Giftcard[]> {
    return this.http.get<Giftcard[]>(this.giftcardsUrl)
  }

  getGiftcard(id: number): Observable<Giftcard> {
    const url = `${this.giftcardsUrl}/${id}`;
    return this.http.get<Giftcard>(url);
  }

  addGiftcard (giftcard: Giftcard): Observable<Giftcard> {
    return this.http.post<Giftcard>(this.giftcardsUrl, giftcard, httpOptions);
  }

  deleteGiftcard (giftcard: Giftcard | number): Observable<Giftcard> {
    const id = typeof giftcard === 'number' ? giftcard : giftcard.id;
    const url = `${this.giftcardsUrl}/${id}`;

    return this.http.delete<Giftcard>(url, httpOptions);
  }

  updateGiftcard (giftcard: Giftcard): Observable<any> {
    return this.http.put(this.giftcardsUrl, giftcard, httpOptions);
  }
}