import { TestBed, inject } from '@angular/core/testing';

import { Picture_typeService } from './picture_type.service';

describe('Picture_typeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Picture_typeService]
    });
  });

  it('should be created', inject([Picture_typeService], (service: Picture_typeService) => {
    expect(service).toBeTruthy();
  }));
});
