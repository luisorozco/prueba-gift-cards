import { Component, OnInit } from '@angular/core';
import { Giftcard } from '../giftcard';
import { GiftcardService } from '../giftcard.service';


@Component({
  selector: 'app-giftcard',
  templateUrl: './giftcard.component.html',
  styleUrls: ['./giftcard.component.css']
})

export class GiftcardComponent  implements OnInit {

  giftcards: Giftcard[];

  constructor(private giftcardService: GiftcardService) {}

  ngOnInit(): void {
     this.getGiftcards();
  }

  getGiftcards() {
    return this.giftcardService.getGiftcards()
               .subscribe(
                giftcards => {
                  console.log(giftcards);
                  this.giftcards = giftcards
                 }
                );
 }
}
