export class Giftcard {
    id: number;
    id_picture: number; 
    amount: number;
    message: string;
    recipient_name: string;
    recipient_email_address: string; 
    recipient_mobile_number: string;
    purchaser_name: string; 
    delivery_date: any;
}
