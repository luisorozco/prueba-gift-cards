import { TestBed, inject } from '@angular/core/testing';

import { GiftcardService } from './giftcard.service';

describe('GiftcardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GiftcardService]
    });
  });

  it('should be created', inject([GiftcardService], (service: GiftcardService) => {
    expect(service).toBeTruthy();
  }));
});
