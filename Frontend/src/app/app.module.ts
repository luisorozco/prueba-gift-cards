import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule }   from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule }     from './app-routing/app-routing.module';

import { AppComponent } from './app.component';

import { GiftcardComponent } from './giftcard/giftcard.component';
import { AddGiftcardComponent } from './add-giftcard/add-giftcard.component';

@NgModule({
  declarations: [
    AppComponent,
    GiftcardComponent,
    AddGiftcardComponent,
      
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
